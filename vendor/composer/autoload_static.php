<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf49eaabc4326c1e2b7b18026be4edf51
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP142711',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf49eaabc4326c1e2b7b18026be4edf51::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf49eaabc4326c1e2b7b18026be4edf51::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
